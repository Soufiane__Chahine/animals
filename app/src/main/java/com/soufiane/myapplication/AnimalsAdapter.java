package com.soufiane.myapplication;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.InterfaceAddress;
import java.util.ArrayList;

public class AnimalsAdapter extends RecyclerView.Adapter<AnimalsAdapter.animalRowHolder> {

    ArrayList<Animal> animalData;
    Context context;
    MyClickInterface myClickInterface;

    public AnimalsAdapter (ArrayList<Animal> animalData, Context context, MyClickInterface myClickInterface) {
        this.animalData = animalData;
        this.context = context;
        this.myClickInterface = myClickInterface;
    }


    @NonNull
    @Override
    public animalRowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.animal_row, viewGroup, false);

        return new animalRowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull animalRowHolder viewHolder, int position) {

        viewHolder.tv__animalName.setText(animalData.get(position).getName());
        viewHolder.iv__animalImage.setImageResource(animalData.get(position).getImage());

    }

    @Override
    public int getItemCount() {
        return animalData.size();
    }

    class animalRowHolder extends RecyclerView.ViewHolder {

        TextView tv__animalName;
        ImageView iv__animalImage;

        public animalRowHolder (@NonNull View itemView) {
            super(itemView);

            tv__animalName = itemView.findViewById(R.id.tv__animalName);
            iv__animalImage = itemView.findViewById(R.id.iv__animalImage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myClickInterface.myItemClick( getAdapterPosition() );
                }
            });

        }

    }

    interface MyClickInterface {

        void myItemClick ( int clickedItemPosition );

    }


}
