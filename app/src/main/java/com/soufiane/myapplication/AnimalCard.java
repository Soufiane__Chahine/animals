package com.soufiane.myapplication;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalCard extends AppCompatActivity {

    ImageView iv__animalImageInfo;
    TextView tv__animalNameInfos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_card);

        iv__animalImageInfo = findViewById(R.id.iv__animalImageInfo);
        tv__animalNameInfos = findViewById(R.id.tv__animalNameInfos);

        Intent recivedIntent = getIntent();

        iv__animalImageInfo.setImageResource(recivedIntent.getIntExtra("image", R.drawable.ic_launcher_foreground));
        tv__animalNameInfos.setText(recivedIntent.getStringExtra("name"));

    }
}