package com.soufiane.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.net.URI;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AnimalsAdapter.MyClickInterface {

    RecyclerView rv__animalsList;
    ArrayList<Animal> animals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv__animalsList = findViewById(R.id.rv__animalsList);
        animals = new ArrayList<>();

        animals.add( new Animal("Owl", R.drawable.owl) );
        animals.add( new Animal("Cat", R.drawable.cat) );
        animals.add( new Animal("Deer", R.drawable.deer) );
        animals.add( new Animal("Dolphin", R.drawable.dolphin) );
        animals.add( new Animal("Dove", R.drawable.dove) );
        animals.add( new Animal("Fox", R.drawable.fox) );
        animals.add( new Animal("Hedgehog", R.drawable.hedgehog) );
        animals.add( new Animal("Horse", R.drawable.horse) );
        animals.add( new Animal("Kingfisher", R.drawable.kingfisher) );
        animals.add( new Animal("Rabbit", R.drawable.rabbit) );
        animals.add( new Animal("Tiger", R.drawable.tiger) );
        animals.add( new Animal("Turtle", R.drawable.turtle) );

        AnimalsAdapter myAnimalsAdapter = new AnimalsAdapter(animals, this, this);

        rv__animalsList.setLayoutManager( new LinearLayoutManager(this));

        rv__animalsList.setAdapter(myAnimalsAdapter);
    }

    @Override
    public void myItemClick(int clickedItemPosition) {

        Intent animalInfoIntent = new Intent(this, AnimalCard.class);
        animalInfoIntent.putExtra("image", animals.get(clickedItemPosition).getImage());
        animalInfoIntent.putExtra("name", animals.get(clickedItemPosition).getName());
        startActivity(animalInfoIntent);

    }
}