package com.soufiane.myapplication;

public class Animal {

    private String name;
    private Integer image;

    public Animal ( String name, Integer image) {
        this.image = image;
        this.name = name;
    }

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }
}
